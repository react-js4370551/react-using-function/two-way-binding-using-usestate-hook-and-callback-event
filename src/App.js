import './App.css';
import { TwoWayBinding } from './component/two-way-binding';

function App() {
  return (
    <div className="App">
      <TwoWayBinding />
    </div>
  );
}

export default App;
