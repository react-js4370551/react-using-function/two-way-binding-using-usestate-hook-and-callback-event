import React, { useState } from 'react'

//two binding means sending data from component to ui and sending back data from ui to component.
//two way binding uses only 'onCHange' befault event that identified the changes in UI.
export function TwoWayBinding() {
  const [name, setName] = useState("ashish");
const updateName=(event)=>{
    setName(event.target.value);
}
  return (
    <div>
        <h1>two-way-binding</h1>
        {/* 
        // when we click data on  updateName then it will update name and again updated 
        value of name will displayed 
        // two way bidning only work on onChange event, not on ther event.
        */}
        <input type='text' onChange={updateName} value={name}/>
        <p>Hi ! {name}</p>
    </div>
  )
}
